import {
  postNewRequest,
  getRequests
} from "./url";

export const fetchRequests = async () => {
  return await request(getRequests());
}

export const newRequest = async (req) => {
  return await fetch(postNewRequest, {
    body: JSON.stringify(req)
  });
}

export const request = async (url) => {
  return fetch(url)
    .then(handleErrors)
    .then((response) => response.json())
    .catch((error) => {
      console.error(error);
    });
};

const handleErrors = (response) => {
  if (!response.ok) throw Error(response.statusText);
  return response;
};
