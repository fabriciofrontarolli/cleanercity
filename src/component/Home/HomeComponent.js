import React, { Component } from "react";
import PropTypes, { string, object } from "prop-types";
import { Button, ScrollView, Text, View, StyleSheet, RefreshControl, TouchableNativeFeedback } from "react-native";

import Screen from "../Screen.js";
import HomeHeader from "./HomeHeader";
import { normalize } from "../../helper/FontSize";
import { orange } from "../../helper/Color";

class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
    };
  }

  onRefresh = async () => {
    this.setState({ isRefreshing: true });
    await this.props.onRefresh();
    this.setState({ isRefreshing: false });
  };

  renderHeader = () => {
    const { navigation, type } = this.props;
    return <HomeHeader navigation={navigation} type={type} />;
  };

  renderTitle = () => {
    return (
      <View>
        <Text style={Styles.screenTitle}>Nova Requisição</Text>
        <View style={Styles.titleBar} />
      </View>
    );
  };

  handleNewRequest = () => {
    const { navigation } = this.props;
    navigation.navigate("RequestDetails", { request });
  }

  renderRequestRow = () => {
    const { navigation, data, subTitle, type, requests } = this.props;
    return requests.map((request, index) => (
      <View key={request.id}>
        <TouchableNativeFeedback onPress={() => navigation.navigate("RequestDetails", { request })}>
          <View style={{ flexDirection: "column", justifyContent: "space-between", alignItems: 'center' }} style={Styles.requestsRow}>
            <Text style={Styles.requestTitle}>
              {request.title}
            </Text>
            <Text style={Styles.requestDate}>{request.date}</Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    ));
  };

  renderRequestsComponent = () => {
    const { isRefreshing } = this.state;
    return (
      <ScrollView
        refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={this.onRefresh} />}
        showsVerticalScrollIndicator={true}
      >
        {this.renderTitle()}
        {this.renderRequestRow()}
      </ScrollView>
    );
  };

  render() {
    return (
      <Screen>
        {this.renderHeader()}
        {this.renderRequestsComponent()}
        <Button title="NOVA SOLICITAÇÃO DE SERVIÇO" color="#ff473a" ></Button>
      </Screen>
    );
  }
}

export default HomeComponent;

HomeComponent.propTypes = {
  navigation: PropTypes.object,
  type: PropTypes.any,
  data: PropTypes.arrayOf(object),
  onRefresh: PropTypes.func,
  subTitle: PropTypes.arrayOf(string),
  requests: PropTypes.arrayOf(object),
};

const Styles = StyleSheet.create({
  screenTitle: {
    fontFamily: "Montserrat-Bold",
    fontSize: normalize(30),
    margin: 16,
    marginBottom: 0,
  },

  titleBar: {
    width: 30,
    height: 5,
    backgroundColor: orange,
    marginTop: 2,
    marginBottom: 12,
    marginLeft: 16,
  },

  requestsRow: {
    marginLeft: 20,
    marginTop: 15,
    marginBottom: 20
  },

  requestTitle: {
    fontWeight: "800",
    fontSize: 24,
    marginBottom: 5
  },

  requestDate: {
    color: '#ff473a',
    textTransform: 'uppercase'
  }
});
