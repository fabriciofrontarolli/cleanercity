import React from "react";
import PropTypes from "prop-types";
import { FlatList, Text, StyleSheet, View } from "react-native";

const MovieList = ({ request, onReachEnd }) => {
  return (
    <View style={{ marginLeft: 24, marginTop: 32 }}>
      <View style={{ marginBottom: 30 }}>
        <Text style={_styles.fieldTitle}>Solicitação</Text>
        <Text style={_styles.fieldValue}>#{request.id} - {request.title}</Text>
      </View>
      <View style={{ marginBottom: 30 }}>
        <Text style={_styles.fieldTitle}>Data</Text>
        <Text style={_styles.fieldValue}>{request.date}</Text>
      </View>
      <View style={{ marginBottom: 30 }}>
        <Text style={_styles.fieldTitle}>Detalhes</Text>
        <Text style={_styles.fieldValue}>{request.description}</Text>
      </View>
    </View>
  );
};

export default MovieList;

MovieList.propTypes = {
  results: PropTypes.array,
  navigation: PropTypes.object,
  type: PropTypes.oneOf(["tv", "movie"]),
  onReachEnd: PropTypes.func,
  request: PropTypes.object
};

MovieList.defaultProps = {
  onReachEnd: null,
};

const _styles = StyleSheet.create({
  fieldTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    borderLeftColor: '#ff473a',
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginBottom: 10
  },
  fieldValue: {
    fontSize: 18,
  },
});
