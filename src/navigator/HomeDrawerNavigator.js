import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Text } from "react-native";

import { black, white, orange } from "../helper/Color";
import NewRequestScreen from "../screen/NewRequest";
import RequestsScreen from "../screen/Requests";

const Drawer = createDrawerNavigator();

const HomeDrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Requisições"
      drawerType={"slide"}
      drawerStyle={{ width: "50%", backgroundColor: black }}
      drawerContentOptions={{
        activeBackgroundColor: "transparent",
        activeTintColor: orange,
        inactiveTintColor: white,
      }}
    >
      <Drawer.Screen
        name="Requisições"
        component={RequestsScreen}
        options={{
          drawerLabel: ({ color, focused }) => CustomDrawerStyle(color, focused, "Requisições"),
        }}
      />
      <Drawer.Screen
        name="Nova Solicitação"
        component={NewRequestScreen}
        options={{
          drawerLabel: ({ color, focused }) => CustomDrawerStyle(color, focused, "Nova Solicitação"),
        }}
      />
    </Drawer.Navigator>
  );
};

const CustomDrawerStyle = (color, focused, title) => {
  return (
    <Text
      style={{
        fontSize: focused ? 20 : 16,
        fontWeight: null,
        color: color,
        fontFamily: focused ? "Montserrat-Bold" : "Montserrat-Light",
      }}
    >
      {title}
    </Text>
  );
};

export default HomeDrawerNavigator;
