import React, { Component, useState } from "react";
import moment from 'moment';
import { Button, View, Text, TextInput, StyleSheet, Picker } from 'react-native';
import PropTypes from "prop-types";

import HomeComponent from "../component/Home/HomeComponent";
import { newRequest } from "../api/api";

class TVShowScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moviesData: [],
      selectedServiceType: '',
      date: moment(),
      description: ''
    };
  }

  componentDidMount() {
    // this.requestMovieScreen();
  }

  handleSetServiceType(service) {
    this.setState({ selectedValue: service });
  }

  handleNewRequest() {
    const req = {
      description: this.state.description,
      type: this.state.selectedServiceType,
      date: this.state.date
    };

    newRequest(req);
  }

  requestMovieScreen = async () => {
    await requestTVShowAPI((data) => this.setState({ moviesData: data }));
  };

  render() {
    return (
      <View style={Styles.container}>
        <View style={Styles.titleContainer}>
          <Text style={Styles.screenTitle}>Nova Solicitação</Text>
        </View>
        <View style={Styles.fieldsContainer}>
          <Text style={Styles.label}>{ this.state.date.format('dddd, MMMM YYYY') }</Text>
          <Text style={Styles.label}>Descrição</Text>
          <TextInput style={{
            height: 100,
            borderColor: 'gray',
            borderWidth: 1,
            borderRadius: 4
          }} />

          <Text style={Styles.label}>Tipo do Serviço</Text>
          <Picker
            selectedValue={this.state.selectedServiceType}
            style={{ height: 50, width: 280 }}
            onValueChange={itemValue => handleSetServiceType(itemValue)}
          >
            <Picker.Item label="Coleta de Lixo" value="coleta" />
            <Picker.Item label="" value="js" />
          </Picker>

          <Button title="NOVA SOLICITAÇÃO" color="#ff473a" onPress={this.handleNewRequest}></Button>
        </View>
      </View>
    );
  }
}

export default TVShowScreen;

TVShowScreen.propTypes = {
  navigation: PropTypes.object,
};

const Styles = StyleSheet.create({
  container: {
    marginTop: 48,
    paddingRight: 10
  },
  titleContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
  },
  screenTitle: {
    fontSize: 24,
    color: '#ff473a',
    marginBottom: 36
  },

  fieldsContainer: {
    marginLeft: 14
  },

  label: {
    fontSize: 16,
    color: '#ff473a',
    marginTop: 15,
    marginBottom: 5
  },
  textInput: {
    borderColor: '#f7f7f7',
    borderWidth: 1
  }
});
