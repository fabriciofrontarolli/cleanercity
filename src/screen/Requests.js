import React, { Component } from "react";
import PropTypes from "prop-types";

import HomeComponent from "../component/Home/HomeComponent";
import { requestRequestsScreen as RequestMovieApi } from "../api/api";
import { MovieTypes } from "../helper/Types";

class MovieScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moviesData: [],
      requests: [
        {
          id: '1',
          title: 'Coleta de Lixo na Avenida Brasil',
          date: '15 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '2',
          title: 'Restauração de Pavimentação - Jardim do Bosque',
          date: '17 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '3',
          title: 'Coleta de Lixo na Avenida Brasil',
          date: '15 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '4',
          title: 'Restauração de Pavimentação - Jardim do Bosque',
          date: '17 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '5',
          title: 'Coleta de Lixo na Avenida Brasil',
          date: '15 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '6',
          title: 'Restauração de Pavimentação - Jardim do Bosque',
          date: '17 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '7',
          title: 'Coleta de Lixo na Avenida Brasil',
          date: '15 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        },
        {
          id: '8',
          title: 'Restauração de Pavimentação - Jardim do Bosque',
          date: '17 de Abril, 2020',
          description: 'Coleta de lixo na rua Francisco Matarazo, nº 5985. Próximo ao clube do centro. O lixo já está sem coleta há uma semana e está incomodando os moradores, por gentileza providenciar a remoção.'
        }
      ]
    };
  }

  componentDidMount() {
    this.requestRequestsScreen();
  }

  requestRequestsScreen = async () => {
    await RequestMovieApi((data) => this.setState({ moviesData: data }));
  };

  render() {
    return (
      <HomeComponent
        type={"movie"}
        subTitle={MovieTypes}
        navigation={this.props.navigation}
        data={this.state.moviesData}
        onRefresh={this.requestRequestsScreen}
        requests={this.state.requests}
      />
    );
  }
}

export default MovieScreen;

MovieScreen.propTypes = {
  navigation: PropTypes.object,
};
